import { Rol } from './rol';
export class Perfil{
    idUsuario: number;
    username: string;
    roles: Rol[];
}