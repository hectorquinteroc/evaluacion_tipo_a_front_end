import { Paciente } from './paciente';
export class Signos{
    idSignos: number;
    paciente: Paciente;
    fecha: string;
    pulso: string;
    ritmoRespiratorio: string;
    temperatura: string;
}