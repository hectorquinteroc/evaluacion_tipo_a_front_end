import { Rol } from './../_model/rol';
import { Subject } from 'rxjs';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Perfil } from './../_model/perfil';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PerfilService  extends GenericService<Perfil>{

  private perfilCambio = new Subject<Perfil[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(
      http,
      `${environment.HOST}/perfil`
    );
  }

    listarRoles(usuario: string) {
      console.log("perfil.service.ts " + usuario);
      let token = sessionStorage.getItem(environment.TOKEN_NAME);

      return this.http.post<Perfil>(`${this.url}/${usuario}`, usuario, {
        headers: new HttpHeaders().set('Authorization', `bearer ${token}`).set('Content-Type', 'application/json')
      });
    }
    //get Subjects
    getPerfilCambio() {
      return this.perfilCambio.asObservable();
    }
  
    getMensajeCambio() {
      return this.mensajeCambio.asObservable();
    }
  
    //set Subjects
    setPerfilCambio(medicos: Perfil[]) {
      this.perfilCambio.next(medicos);
    }
  
    setMensajeCambio(mensaje: string) {
      this.mensajeCambio.next(mensaje);
    }
}
