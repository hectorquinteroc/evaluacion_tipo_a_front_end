import { Rol } from './../../_model/rol';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PerfilService } from './../../_service/perfil.service';
import { MatTableDataSource } from '@angular/material/table';
import { Perfil } from './../../_model/perfil';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  edicion: boolean;
  form: FormGroup;
  usuario: string;
  public roles: Rol[];
  public username: string;

  displayedColumns = ['usuario', 'rol'];
  dataSource: MatTableDataSource<Perfil>;

  constructor(    
    private perfilService: PerfilService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'usuario': new FormControl(''),
      'rol': new FormControl(0)
    });

    
      this.usuario = sessionStorage.getItem('User');
      console.log(this.usuario);
      this.edicion = sessionStorage.getItem('User') != null;
      console.log(this.edicion);
      this.initForm();
    
  }


  private initForm() {
    if (this.edicion) {

      this.perfilService.listarRoles(this.usuario).subscribe(data => {
        this.roles = data.roles;
        this.username = data.username;
        /*this.form = new FormGroup({
          'usuario': new FormControl(data.username),
          'rol': new FormControl(this.roles)
        });*/
      });
    }
  }
}
