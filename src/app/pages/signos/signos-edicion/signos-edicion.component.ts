import { Paciente } from './../../../_model/paciente';
import { PacienteService } from './../../../_service/paciente.service';
import { switchMap, map } from 'rxjs/operators';
import { Signos } from './../../../_model/signos';
import { SignosService } from './../../../_service/signos.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean;

  idPacienteSeleccionado: number;

  pacientes: Paciente[];
  pacientes$: Observable<Paciente[]>;

  maxFecha: Date = new Date();
  fechaSeleccionada: Date = new Date();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private signosService: SignosService,
    private pacienteService: PacienteService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'fecha': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl(''),
      'temperatura': new FormControl(''),
      'paciente': new FormControl('')
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      console.log('ngOnInit ' + this.edicion);
      this.initForm();
    });

    this.pacientes$ = this.pacienteService.listar();
  }
  get f() { return this.form.controls; }
  
  private initForm() {
    if (this.edicion) {

      this.signosService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSignos),
          'paciente': new FormControl(data.paciente.nombres),
          'fecha': new FormControl(data.fecha),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio),
          'temperatura': new FormControl(data.temperatura)
        });
      });
    }
  }

  cambieFecha(e: any) {
    console.log(e);
  }

  operar() {

    if (this.form.invalid) { return; }

    let signos = new Signos();
    let paciente = new Paciente();
    paciente.idPaciente = this.idPacienteSeleccionado;
    signos.idSignos = this.form.value['id'];
    signos.paciente = paciente;
    signos.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    //signos.fecha = this.form.value['fecha'];
    signos.pulso = this.form.value['pulso'];
    signos.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];
    signos.temperatura = this.form.value['temperatura'];
    if (this.edicion) {
      console.log('SE MODIFICA ' + this.edicion);
      this.signosService.modificar(signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajecambio('SE MODIFICÓ');
      });
    } else {
      console.log('SE REGISTRA ' + this.edicion);
      this.signosService.registrar(signos).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajecambio('SE REGISTRÓ');
        });
      });
    }

    this.router.navigate(['signos']);
  }
}
